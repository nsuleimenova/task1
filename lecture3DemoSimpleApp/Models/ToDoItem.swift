//
//  ToDoItem.swift
//  lecture3DemoSimpleApp
//
//  Created by admin on 08.01.2021.
//

import Foundation

public class ToDoItem {
    let id: Int?
    let title: String?
    let subtitle: String?
    let deadLine: String?
    
    init(id: Int, title: String, subtitle: String, deadLine: String) {
        self.id = id
        self.title = title
        self.subtitle = subtitle
        self.deadLine = deadLine
    }
}
