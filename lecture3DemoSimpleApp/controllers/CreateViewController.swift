//
//  CreateViewController.swift
//  lecture3DemoSimpleApp
//
//  Created by Асем on 20.01.2021.
//

import UIKit

class CreateViewController: ViewController {
    @IBOutlet var textField: UITextField!
    @IBOutlet var bodyField: UITextField!
    @IBOutlet var deadLine: UITextField!

    public var complition: ((String, String, String) -> Void)?


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func didTapSaveButton(){

            if let titleText = textField.text, !titleText.isEmpty,
               let bodyText = bodyField.text, !bodyText.isEmpty,
               let deadLineText = deadLine.text, !deadLineText.isEmpty
               {
                complition?(titleText, bodyText, deadLineText)
}
}
}
