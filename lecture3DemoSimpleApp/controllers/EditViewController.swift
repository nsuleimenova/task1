//
//  EditViewController.swift
//  lecture3DemoSimpleApp
//
//  Created by Асем on 20.01.2021.
//

import UIKit

class EditViewController: ViewController {

    @IBOutlet var textField: UITextField!
    @IBOutlet var bodyField: UITextField!
    @IBOutlet var deadLine: UITextField!

    public var complition: ((String, String, String) -> Void)?
    public var todo: ToDoItem?

    override func viewDidLoad() {
        super.viewDidLoad()
        textField.text = todo?.title
        bodyField.text = todo?.subtitle
        deadLine.text = todo?.deadLine
    }
    
    @IBAction func didTapSaveButton(){

            if let titleText = textField.text, !titleText.isEmpty,
               let bodyText = bodyField.text, !bodyText.isEmpty,
               let deadLineText = deadLine.text, !deadLineText.isEmpty
               {
                complition?(titleText, bodyText, deadLineText)
}
}

}
