//
//  SecondViewController.swift
//  lecture3DemoSimpleApp
//
//  Created by admin on 06.01.2021.
//

import UIKit

/*protocol SecondViewControllerDelegate {
    func removeItem(id: IndexPath) -> UIContextualAction
    func editItem(_ id: Int)
}*/

class SecondViewController: UIViewController {
   
    
    
    @IBOutlet weak var tableView: UITableView!
    var arr = [ToDoItem]()
    let cellId = "TableViewCell"
    //var delegate: SecondViewControllerDelegate?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
        //testDataConfigure()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
     //   tableView.delegate = self
       // tableView.dataSource = self
        self.title = "Main page"
        self.configureTableView()
    }
    
    
    /*func testDataConfigure(){
        arr.append(ToDoItem(id: 1, title: "first",subtitle: "1", deadLine: "20.12.2021"))
        arr.append(ToDoItem(id: 2, title: "second",subtitle: "1", deadLine: "22.12.2021"))
        arr.append(ToDoItem(id: 3, title: "third",subtitle: "1", deadLine: "12.08.2021"))
        arr.append(ToDoItem(id: 4, title: "uno", subtitle: "1",deadLine: "05.07.2021"))
        arr.append(ToDoItem(id: 5, title: "dos",subtitle: "1", deadLine: "29.12.2021"))
    }*/
    
    
    func configureTableView(){
        tableView.delegate = self
       tableView.dataSource = self
        //delegate = self
        tableView.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        tableView.tableFooterView = UIView()
        
    }
    func removeItem(_ index: Int) {
            arr.remove(at: index)
        }
    
    
    @IBAction func add(_ sender: UIBarButtonItem) {
        guard let vc = storyboard?.instantiateViewController(identifier: "create") as? CreateViewController else{
             return
                }
        vc.title = "Add item"
                vc.navigationItem.largeTitleDisplayMode = .automatic
                vc.complition = {title, body, deadline in
                    DispatchQueue.main.async {
                        self.navigationController?.popViewController(animated: true)
                        let new = ToDoItem(id: self.arr.count, title: title, subtitle : body,  deadLine: deadline)
                        self.arr.append(new)
                        self.tableView.reloadData()
                    }
                }

 navigationController?.pushViewController(vc, animated: true)
tableView.reloadData()
    }




/*extension SecondViewController: SecondViewControllerDelegate{
    func editItem(_ id: Int) {
        <#code#>
    }
    
    func removeItem(id: IndexPath) -> UIContextualAction {
        let delete = UIContextualAction(style: .destructive, title: "delete") {
            (delete,_, _) in
            self.arr.remove(at: id.item)
            self.tableView.deleteRows(at: [id], with: .none)
            
        }
            return delete
        }
    */
  /*  func editItem(_ id: IndexPath) -> UIContextualAction {
        //open new view controller which allows you to change the data of the array item
        let edit = storyboard?.instantiateViewController(identifier: "EditViewController") as! SecondViewController
        navigationController?.pushViewController(vc, animated: true)
    }*/
//}
    
}

extension SecondViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! TableViewCell
        let item = arr[indexPath.row]
       // cell.delegate = self
        cell.id = item.id ?? 0
        cell.titleLabel.text = item.title
        //cell.deadlineLabel.text = item.deadLine
        cell.subTitleLabel.text = item.deadLine
        
        
        
return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            tableView.deselectRow(at: indexPath, animated: true)
            
            
            guard let vc = self.storyboard?.instantiateViewController(identifier: "edit") as? EditViewController
            else{
                return
            }
            
            vc.title = "Edit cell"
            vc.todo = self.arr[indexPath.row]
            vc.navigationItem.largeTitleDisplayMode = .never
            vc.complition = {title, body, deadline in
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                
                    let new = ToDoItem(id: vc.todo?.id ?? 0, title: title, subtitle: body, deadLine: deadline)
                
                    self.arr[indexPath.row] = new
                
                    self.tableView.reloadData()
                }
                        }
            self.navigationController?.pushViewController(vc, animated: true)



        }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = deleteAction(at : indexPath)
                
                return UISwipeActionsConfiguration(actions: [delete])
    }
    func deleteAction(at indexPath: IndexPath) -> UIContextualAction {
            let action = UIContextualAction(style: .destructive, title: "Delete") { (action, view, completion) in
                self.removeItem(indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
                completion(true)
            }
            action.backgroundColor = .red
            return action
        }
    
    
}
    
    /*func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let delete = removeItem(id: indexPath)
        
        
        
        return UISwipeActionsConfiguration(actions: [delete])
        
    }*/
    /*func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let edit = editItem(id: indexPath)
        
        
        
        return UISwipeActionsConfiguration(actions: [edit])
        
     }*/

    

